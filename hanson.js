//***************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20
//***************************************************** HTTPS server setup
//-----* Express inicia servidor / carpeta raiz
//------------------------------------Express inicia servidor 
const express = require('express')
const app = express()
const fs = require('fs')
//const ImageDataURI = require('image-data-uri')
app.use(express.static(__dirname))//Carpeta de donde sirve / carpeta raiz public

const server = app.listen(8888, () => {
    console.log('Servidor web iniciado')
})

//-----* Filesystem module object
var fss = require('fs')
//-----* https module object
var https = require('https')

//***************************************************** Seccion Socket 
var io = require('socket.io')(server)

io.on('connection', (socket) => {

    socket.on('ejemplo', async function () { await socketfunction() })

})//close io.on

//***************************************************** Postgres module object
const { Client } = require('pg')
postgresdb = new Client({
    host: 'localhost',
    user: 'app_aquiles',
    password: 'Redsamuraix1.',
    database: 'AquilesDB',
})

//***************************************************** Files Handler
io.on('connection', (socket) => {
    socket.on('deletefile', function (path) {
        deletef(path);
    }); //Close socket

    socket.on('picsaving', async function (datauri,serial,sqty){ 
        await savingpic(datauri,serial,sqty);
        console.log("recibe",serial,qty);
    });

    socket.on('logsaving', async function (sn, logsaving) { // Funcion de ejemplo borrar no importante
        savinglog(sn, logsaving)
    });
    socket.on('renombrasnr', function (snr) { // conexion con main_script
        renombraF(snr);
    });

});
async function deletef(path) {
    await deletefile(path);
}

async function deletefile(path) {

    return new Promise(async resolve => {

        netpath = path;
        fs.unlinkSync(netpath)
        console.log("Archivo borrado: " + netpath);

    });//Cierra Promise
}

//-----* Guarda imagen desde URI
async function savingpic(datauri, serial, sqty) {

    let filePath;
    const ImageDataURI = require('image-data-uri');
    return new Promise(async resolve => {
        //console.log("Variables:"+serial+' - '+sqty+'');// temporal para ver que esta rebiendo 

        //C:/Users/mayra_ayala/Documents/Aquiles/img/
        //C:/Users/gdl3_mds/myapp/timsamples/
        let filePath = 'C:/Users/mayra_ayala/Documents/Projects_Git/sofia/img/test/' + serial + '';//Ruta de las carpetas por serial
        let filevalidation = fs.existsSync(filePath);

        if (filevalidation) {

            filePath = '' + filePath + '/' + sqty + '';
            ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
        }
        else {
            fs.mkdir(filePath, (error) => {
                if (error) {
                    console.log(error.message);//en caso de que el folder ya exista manda un error y evita hacer otro folder con el mismo nombre.
                }
                filePath = '' + filePath + '/' + sqty + '';
                ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
                console.log("Directorio creado");
            });
        }
    });//Cierra Promise
}

//Funcion para renombrar carpeta F 
async function renombraF(serial) {
        fs.rename('C:/Users/mayra_ayala/Documents/Projects_Git/sofia/img/test/' + serial,
            'C:/Users/mayra_ayala/Documents/Projects_Git/sofia/img/test/' + serial + '_F',
            function (err) {
                if (err)
                    console.log('Error de renombramiento');
            });
}

function savinglog(sn, logdata) {

        let logpath = 'C:/Users/gdl3_mds/myapp/timsamples/' + sn + '/log.txt';
        fs.writeFile(logpath, logdata, function (err) {
            if (err) throw err;
        });
}

//***************************************************** TCP/IP PLC TESLA
let plc_endresponse = 0
io.on('connection', (socket) => {

    socket.on('plc_response', function (result_matrix) {
        plcdatasender(result_matrix)
    })

})

var net = require('net')
var tcpipserver = net.createServer(function (connection) {
    console.log('TCP client connected')

    connection.write('Handshake ok!\r\n')

    connection.on('data', function (data) { io.emit('Timsequence_start', data.toString()); console.log("Analisis in process...") })

    //Responde a PLC cuando termine inspeccion
    setTimeout(function respuesta() {
        estadoconexion = connection.readyState
        console.log("Comunicacion con el plc :" + connection.readyState)

        if (estadoconexion == 'closed') {
            console.log("Puerto de PLC cerrado reintento en 1min...")
        }
        if (estadoconexion == 'open') {
            connection.write(plc_endresponse)
        }

    }, 40000) // tiempo de secuencia completa 40s para responder
})

function plcdatasender(result_matrix) {
    matrixtostring = result_matrix.toString()
    plc_endresponse = matrixtostring
}

tcpipserver.listen(40000, function () {
    console.log('PLC Port 40000 listening...')
})